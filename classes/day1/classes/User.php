<?php
class User
{
	private $first_name;
	private $last_name;

	public function printMessage(){
		$msg= "\nHello, world! I am " . $this->first_name . "\n";
		echo $msg;
	}

	public function setFirstName($name)
	{
		 $this->first_name = $name;
	}


	public function getFirstName()
	{
		echo $this->first_name . " ";
	}
	public function setLastName($name)
	{
		$this->last_name = $name;
	}


	public function getLastName()
	{
		echo  $this->last_name ;
	}

	public function __toString()
	{
		$a = $this->first_name;
		$b=$this->last_name;
		return $a . $b;
	}
}