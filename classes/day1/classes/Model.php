<?php
class Model
{
	private $dbh;
	private $table;

	public function __construct($dbh, $table=null)
	{
		$this->dbh=$dbh;
		$this->table=$table;
	}

	public function __toString()
	{
		return __CLASS__;
	}

	public function __invoke()
	{
		return 'hello from invoke';
	}

	public function __set($attr, $val)
	{
		$this->{$attr}=$val;
	}

}