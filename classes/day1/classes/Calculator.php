<?php

class Calculator
{
	private $result = 0;
	public function add($num)
	{
		$this->result+=$num;
		return $this;
	}
	public function sub($num)
	{
		$this->result-=$num;
		return $this;
	}
	public function mul($num)
	{
		$this->result*=$num;
		return $this;
	}
	public function div($num)
	{
		if($num ==0){
			throw new Exception('Cannot divide by zero');
		}
		$this->result/=$num;
		return $this;
	}

	public function result()
	{
		return $this->result;
	}

}