<?php

class Car2
{
	public $make;
	public $model;
	public $color ;
	public $sunroof;	
	private $wheels ;
	private $price;

	public function setPrice($price)
	{
		$this->price = $price;
	}


	public function getPrice()
	{
		return $this->price ;
	}

	public function setWheels($wheels)
	{
		$this->wheels = $wheels;
	}


	public function getWheels()
	{
		return $this->wheels ;
	}

}