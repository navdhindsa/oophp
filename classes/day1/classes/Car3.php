<?php

class Car3 extends Vehicle
{
	private $make;
	private $model;
	private $color ;
	
	public function __construct($type, $make, $model, $color)
	{
		parent::__construct($type);
		$this->color=$color;
		$this->make=$make;
		$this->model=$model;
	}
}